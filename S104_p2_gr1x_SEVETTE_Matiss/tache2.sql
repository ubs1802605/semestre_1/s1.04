-- Test de contrainte : existence de la cle primaire (nomFiliere, numeroINE)
INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Informatique', 1);
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE1', 'John', 'Doe', 20, 1, 'Générale');



-- Test de contrainte : unicite de la cle primaire (numeroVoeu)
INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Info', 0);
INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Mathematiques', 1);
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Info gestion', 'Informatique');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Info industr', 'Informatique');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Mathé appliq', 'Mathematiques');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('123456789', 'John', 'Doe', 20, 0, 'Générale');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('987654321', 'Jane', 'Smith', 21, 1, 'Technologique');
INSERT INTO Etablissement (numEtablissement, estPrive) VALUES ('ETU1', 0);
INSERT INTO Etablissement (numEtablissement, estPrive) VALUES ('ETU2', 1);

INSERT INTO Voeu (numeroVoeu, resultat, dateAppel, dateAcceptation, unEleve, unEtablissementVoeu, uneMentionVoeu, uneFiliereVoeu) VALUES (1, 'Non Classé', '2021-01-01', '2021-02-01', '123456789', 'ETU1', 'Info gestion', 'Info');
INSERT INTO Voeu (numeroVoeu, resultat, dateAppel, dateAcceptation, unEleve, unEtablissementVoeu, uneMentionVoeu, uneFiliereVoeu) VALUES (1, 'Classé Non Appelé', '2021-01-01', '2021-02-01', '987654321', 'ETU2', 'Info industr', 'Info');



-- Test de contrainte : unicite de la cle candidate (nom, prenom)
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE2', 'John', 'Doe', 25, 0, 'Technologique');



-- Test de contrainte : integrite referentielle de la cle etrangere
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Mention1', 'Informatique');
INSERT INTO Etablissement (numEtablissement, estPrive) VALUES ('ETAB1', 1);



-- Test de contrainte : CHECK de valeurs positives de l’attribut age
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0003', 'Jane', 'Doe', -5, 0, 'Professionnelle');



-- Test de contrainte : CHECK de DOM de l’attribut voie
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0004', 'Bob', 'Smith', 30, 1, 'Autre');
