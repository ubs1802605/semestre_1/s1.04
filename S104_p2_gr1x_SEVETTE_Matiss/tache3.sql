INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Informatique', 1);
INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Mathématiques', 0);
INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Sciences', 1);
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Informatique gestion', 'Informatique');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Informatique indus', 'Informatique');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Mathématiques appli', 'Mathématiques');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Physique', 'Sciences');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Chimie', 'Sciences');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0001', 'Martin', 'Dupont', 20, 0, 'Professionnelle');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0002', 'Jean', 'Bombeur', 18, 1, 'Technologique');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0005', 'Fromage', 'Dechevre', 19, 1, 'Technologique');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0006', 'Martin', 'Deutroi', 19, 1, 'Technologique');


--Quels sont les noms et prénoms des élèves qui ont choisi une voie professionnelle?
SELECT DISTINCT nom, prenom FROM Eleve WHERE UPPER(voie) = 'PROFESSIONNELLE';
--Algèbre relationnelle: Eleve{voie = 'Professionnelle'}[nom, prenom]



--Quels sont les numeroINE des élèves qui ont choisi une voie Technologiques ?
SELECT DISTINCT numeroINE FROM Eleve WHERE UPPER(voie) = 'TECHNOLOGIQUE';
--Algèbre relationnelle: Eleve{voie = 'Technologique'}[numeroINE]



--Quels sont les numeroINE des élèves qui ont tout sauf 18 ans et en voix technologique ?
SELECT DISTINCT numeroINE FROM Eleve WHERE voie = 'Technologique' 
MINUS
SELECT DISTINCT numeroINE FROM Eleve WHERE age = 18;
--Algèbre relationnelle: Eleve{voie = 'Technologique'}[numeroINE] / Eleve{age = 18}[numeroINE]



--Quels sont les numeroINE des élèves qui sont en voie technologique ou en voie professionnelle ?
SELECT DISTINCT numeroINE FROM Eleve WHERE UPPER(voie) = 'TECHNOLOGIQUE' 
UNION
SELECT DISTINCT numeroINE FROM Eleve WHERE UPPER(voie) = 'PROFESSIONNELLE';
--Algèbre relationnelle: Eleve{voie = 'Technologique'}[numeroINE] U Eleve{voie = 'Professionnelle'}[numeroINE]



--Quel sont les INE des élèves qui s'appellent Martin et qui ont 20 ans ?
SELECT DISTINCT numeroINE FROM Eleve WHERE UPPER(nom) = 'MARTIN' 
INTERSECT
SELECT DISTINCT numeroINE FROM Eleve WHERE age = 20;
--Algèbre relationnelle: Eleve{voie = 'Technologique'}[numeroINE] U Eleve{voie = 'Professionnelle'}[numeroINE]



--Quels sont les intitulés des mentions triés par ordre alphabétique ?
SELECT DISTINCT intituleMention FROM Mention
ORDER BY intituleMention;
--Algèbre relationnelle: Mention[intituleMention>]



--Quels sont les intitulés des mentions et les noms des filières triés par ordre alphabétique des intitulés de mention?
SELECT DISTINCT intituleMention, uneFiliere FROM Mention
ORDER BY intituleMention;
--Algèbre relationnelle: Mention[intituleMention][uneFiliere](intituleMention>)



--Quels sont les 3 premières intitulés des mentions triés par ordre alphabétique ?
SELECT *
FROM (
    SELECT DISTINCT intituleMention
	FROM Mention
	ORDER BY intituleMention
)
WHERE ROWNUM <= 3;
--Algèbre relationnelle: Mention[ROWNUM<=3][intituleMention](intituleMention>)



--Jointure de Filiere et Mention
SELECT *
FROM Filiere, Mention
WHERE uneFiliere = nomFiliere;
--Algèbre relationnelle: Filiere * Mention



--Jointure de Filiere et Mention et Eleve
SELECT *
FROM Filiere, Mention, Eleve
WHERE uneFiliere = nomFiliere
AND nom = 'John';
--Algèbre relationnelle: Filiere * Mention * Eleve{age > 18}



--Autojointure Eleve
-- Quel prenom des eleves qui s'appellent Martin n'est pas boursier ?
SELECT DISTINCT e2.prenom
FROM Eleve e1, Eleve e2
WHERE e1.nom = 'Martin'
AND e2.nom = 'Martin'
AND e2.estBoursier = 0;
--Algèbre relationnelle: Eleve e1 * Eleve e2{e1.nom = 'Martin' AND e2.nom = 'Martin' AND e2.estBoursier = 0}
 
