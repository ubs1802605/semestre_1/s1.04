-- =================== SAE S104 Partie 2 =========================
-- Schéma relationnel --
/*
Filiere(
	nomFiliere (1),
	estSelective (NN)
)
Mention (
	intituleMention (1),
	uneFiliere=@Filiere(nomFiliere) (1)
)
Eleve(
	numeroINE (1),
	nom (2),
	prenom (2),
	age,
	estBoursier,
	voie
)
Etablissement(
	numEtablissement (1),
	estPrive
)
Voeu(
	numeroVoeu (1),
	resultat (NN),
	dateAppel,
	dateAcceptation,
	unEleve=@Eleve(numeroINE) (NN),
	unEtablissementVoeu=@Etablissement(numEtablissement) (NN),
	(uneMentionVoeu,uneFiliereVoeu) = @Mention(intituleMention,uneFiliere) (NN)
)
Proposition(
	unEtablissementP = @Etablissement(numEtablissement) (1),
	(uneMentionP,uneFiliereP) = @Mention(intituleMention,uneFiliere) (1)
)

-- Contraintes textuelles:
1) DOM(voie) = {'Générale','Technologique','Professionnelle'}
2) DOM(resultat) = {'Non Classé','Classé Non Appelé','Classé Appelé'}

-- Remarque: pour déclarer les attributs de type Boolean, on utilise
le type NUMBER avec une vérification de valeur (0 ou 1)
*/ 

DROP TABLE Proposition;
DROP TABLE Voeu;
DROP TABLE Etablissement;
DROP TABLE Eleve;
DROP TABLE Mention;
DROP TABLE Filiere;



CREATE TABLE Filiere (
    nomFiliere VARCHAR(20) 
        CONSTRAINT pk_Filiere PRIMARY KEY,
    estSelective NUMBER(1) 
        CONSTRAINT ck_Filiere_ischk CHECK (estSelective IN (0, 1))
        CONSTRAINT nn_estSelective NOT NULL
);

CREATE TABLE Mention (
    intituleMention VARCHAR(20),
    uneFiliere VARCHAR(20) 
        CONSTRAINT fk_Mention REFERENCES Filiere(nomFiliere),
    CONSTRAINT pk_Mention PRIMARY KEY (intituleMention, uneFiliere)
);

CREATE TABLE Eleve (
    numeroINE VARCHAR(20) 
        CONSTRAINT pk_Eleve PRIMARY KEY,
    nom VARCHAR(20) 
        CONSTRAINT nn_nom NOT NULL,
    prenom VARCHAR(20) 
        CONSTRAINT nn_prenom NOT NULL,
    age NUMBER,
    estBoursier NUMBER(1) 
        CONSTRAINT ck_Eleve_ischk CHECK (estBoursier IN (0, 1)),
    voie VARCHAR(20) 
        CONSTRAINT ck_Eleve_voie CHECK (voie IN ('Générale', 'Technologique', 'Professionnelle')),
);

CREATE TABLE Etablissement (
numEtablissement VARCHAR(20)
    CONSTRAINT pk_Etablissement PRIMARY KEY,
estPrive NUMBER(1)
    CONSTRAINT ck_Etablissement_ischk CHECK (estPrive IN (0, 1))
);

CREATE TABLE Voeu (
    numeroVoeu NUMBER
        CONSTRAINT pk_Voeu PRIMARY KEY,
    resultat VARCHAR(20)
        CONSTRAINT ck_Voeu_resultat CHECK (resultat IN ('Non Classé', 'Classé Non Appelé', 'Classé Appelé'))
        CONSTRAINT nn_resultat NOT NULL,
    dateAppel DATE,
    dateAcceptation DATE,
    unEleve VARCHAR(20)
        CONSTRAINT fk_Voeu_Eleve REFERENCES Eleve(numeroINE)
        CONSTRAINT nn_unEleve NOT NULL,
    unEtablissementVoeu VARCHAR(20)
        CONSTRAINT fk_Voeu_Etablissement REFERENCES Etablissement(numEtablissement)
        CONSTRAINT nn_unEtablissementVoeu NOT NULL,
    uneMentionVoeu VARCHAR(20)
        CONSTRAINT nn_uneMentionVoeu NOT NULL,
    uneFiliereVoeu VARCHAR(20)
        CONSTRAINT nn_uneFiliereVoeu NOT NULL,
    CONSTRAINT fk_Voeu_Mention FOREIGN KEY (uneMentionVoeu, uneFiliereVoeu) REFERENCES Mention(intituleMention, uneFiliere)

);

CREATE TABLE Proposition (
    unEtablissementP VARCHAR(20)
        CONSTRAINT fk_Proposition_Etablissement REFERENCES Etablissement(numEtablissement),
    uneMentionP VARCHAR(20),
    uneFiliereP VARCHAR(20),
    
    CONSTRAINT fk_Proposition_Mention FOREIGN KEY (uneMentionP, uneFiliereP) REFERENCES Mention(intituleMention, uneFiliere),
    CONSTRAINT pk_Proposition PRIMARY KEY (unEtablissementP, uneMentionP, uneFiliereP)
);


ALTER TABLE Etablissement ADD ville VARCHAR(20);
--supprimer la colonne 
ALTER TABLE Etablissement DROP COLUMN ville;
--ajouter une contrainte unique sur nom dans eleve
ALTER TABLE Eleve ADD CONSTRAINT uk_Eleve_nom UNIQUE (nom);
--supprimer une contrainte unique sur nom dans eleve
ALTER TABLE Eleve DROP CONSTRAINT uk_Eleve_nom;








INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Informatique', 1);
INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Mathématiques', 0);
INSERT INTO Filiere (nomFiliere, estSelective) VALUES ('Sciences', 1);

INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Informatique gestion', 'Informatique');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Informatique indus', 'Informatique');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Mathématiques appli', 'Mathématiques');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Physique', 'Sciences');
INSERT INTO Mention (intituleMention, uneFiliere) VALUES ('Chimie', 'Sciences');

INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0001', 'Martin', 'Dupont', 20, 0, 'Professionnelle');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0002', 'Jean', 'Bombeur', 18, 1, 'Technologique');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0005', 'Fromage', 'Dechevre', 19, 1, 'Technologique');
INSERT INTO Eleve (numeroINE, nom, prenom, age, estBoursier, voie) VALUES ('INE0006', 'Martin', 'Deutroi', 19, 1, 'Technologique');

--Quels sont les noms et prénoms des élèves qui ont choisi une voie professionnelle?
SELECT DISTINCT nom, prenom FROM Eleve WHERE UPPER(voie) = 'PROFESSIONNELLE';
--Algèbre relationnelle: Eleve{voie = 'Professionnelle'}[nom, prenom]

--Quels sont les numeroINE des élèves qui ont choisi une voie Technologiques ?
SELECT DISTINCT numeroINE FROM Eleve WHERE UPPER(voie) = 'TECHNOLOGIQUE';
--Algèbre relationnelle: Eleve{voie = 'Technologique'}[numeroINE]

--Quels sont les numeroINE des élèves qui ont tout sauf 18 ans et en voix technologique ?
SELECT DISTINCT numeroINE FROM Eleve WHERE voie = 'Technologique' 
MINUS
SELECT DISTINCT numeroINE FROM Eleve WHERE age = 18;
--Algèbre relationnelle: Eleve{voie = 'Technologique'}[numeroINE] / Eleve{age = 18}[numeroINE]

--Quels sont les numeroINE des élèves qui sont en voie technologique ou en voie professionnelle ?
SELECT DISTINCT numeroINE FROM Eleve WHERE UPPER(voie) = 'TECHNOLOGIQUE' 
UNION
SELECT DISTINCT numeroINE FROM Eleve WHERE UPPER(voie) = 'PROFESSIONNELLE';
--Algèbre relationnelle: Eleve{voie = 'Technologique'}[numeroINE] U Eleve{voie = 'Professionnelle'}[numeroINE]

--Quel sont les INE des élèves qui s'appellent Martin et qui ont 20 ans ?
SELECT DISTINCT numeroINE FROM Eleve WHERE UPPER(nom) = 'MARTIN' 
INTERSECT
SELECT DISTINCT numeroINE FROM Eleve WHERE age = 20;
--Algèbre relationnelle: Eleve{voie = 'Technologique'}[numeroINE] U Eleve{voie = 'Professionnelle'}[numeroINE]

--Quels sont les intitulés des mentions triés par ordre alphabétique ?
SELECT DISTINCT intituleMention FROM Mention
ORDER BY intituleMention;
--Algèbre relationnelle: Mention[intituleMention>]

--Quels sont les intitulés des mentions et les noms des filières triés par ordre alphabétique des intitulés de mention?
SELECT DISTINCT intituleMention, uneFiliere FROM Mention
ORDER BY intituleMention;
--Algèbre relationnelle: Mention[intituleMention][uneFiliere](intituleMention>)

--Quels sont les 3 premières intitulés des mentions triés par ordre alphabétique ?
SELECT *
FROM (
    SELECT DISTINCT intituleMention
	FROM Mention
	ORDER BY intituleMention
)
WHERE ROWNUM <= 3;
--Algèbre relationnelle: Mention[ROWNUM<=3][intituleMention](intituleMention>)

--Jointure de Filiere et Mention
SELECT *
FROM Filiere, Mention
WHERE uneFiliere = nomFiliere;
--Algèbre relationnelle: Filiere * Mention

--Jointure de Filiere et Mention et Eleve
SELECT *
FROM Filiere, Mention, Eleve
WHERE uneFiliere = nomFiliere
AND nom = 'John';
--Algèbre relationnelle: Filiere * Mention * Eleve{age > 18}

--Autojointure Eleve
-- Quel prenom des eleves qui s'appellent Martin n'est pas boursier ?
SELECT DISTINCT e2.prenom
FROM Eleve e1, Eleve e2
WHERE e1.nom = 'Martin'
AND e2.nom = 'Martin'
AND e2.estBoursier = 0;
--Algèbre relationnelle: Eleve e1 * Eleve e2{e1.nom = 'Martin' AND e2.nom = 'Martin' AND e2.estBoursier = 0}
